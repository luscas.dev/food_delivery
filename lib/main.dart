import 'package:flutter/material.dart';
import 'package:food_delivery/constants.dart';
import 'package:food_delivery/screens/cart/index.dart';

void main() => runApp(MaterialApp(
  title: 'Food Delivery',
  debugShowCheckedModeBanner: false,
  theme: ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    primarySwatch: ThemeColor.primary,
    fontFamily: 'Open Sans',
  ),
  initialRoute: 'home',
  routes: {
    'home': (_) => HomePage(),
    'cart' : (_) => CartPage(),
  },
));


class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: () => Navigator.of(context).pushNamed('cart'),
          child: Text('CART PAGE'),
        )
      )
    );
  }
}