import 'package:flutter/material.dart';

class ThemeColor {
  static MaterialColor primary = MaterialColor(0xFF4E60FF, <int, Color> {
    50: Color(0xFFEAECFF),
    100: Color(0xFFCACFFF),
    200: Color(0xFFA7B0FF),
    300: Color(0xFF8390FF),
    400: Color(0xFF6978FF),
    500: Color(0xFF4E60FF),
    600: Color(0xFF4758FF),
    700: Color(0xFF3D4EFF),
    800: Color(0xFF3544FF),
    900: Color(0xFF2533FF)
  });
  static Color grey = Color(0xFF061737);
  static Color greyVariant = Color(0xFF898EBC);
}