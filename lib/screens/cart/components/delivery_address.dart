import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'package:flutter_remix/flutter_remix.dart';
import 'package:food_delivery/constants.dart';

Widget deliveryAddress(bool show) => AnimatedContainer(
  duration: Duration(milliseconds: 700),
  curve: Curves.easeInQuad,
  transform: Matrix4.translationValues(0, show ? 0 : 500, 0),
  child: OpenContainer(
    closedElevation: 0.0,
    openElevation: 0.0,
    closedBuilder: (_, c) => Material(
      borderRadius: BorderRadius.circular(15.0),
      color: Color(0xFFEDEEF5),
      child: InkWell(
        borderRadius: BorderRadius.circular(15.0),
        focusColor: Color(0xFFEDEEF5),
        highlightColor: Color(0xFFE3E4EC),
        child: Padding(
          padding: EdgeInsets.only(top: 22.0, left: 22.0, right: 22.0, bottom: 42.0),
          child: Row(
            children: [
              Icon(FlutterRemix.map_pin_line, color: ThemeColor.primary),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Wisteria st 30, Houston, TX', style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.w600,
                        color: ThemeColor.grey,
                      )),
                      SizedBox(height: 3.0),
                      Text('MR. Smith +555 9867 4321', style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400,
                        color: ThemeColor.greyVariant,
                      )),
                    ],
                  ),
                ),
              ),
              Icon(FlutterRemix.arrow_right_s_line, color: ThemeColor.primary)
            ],
          ),
        ),
      ),
    ),
    openBuilder: (_, o) => Scaffold(
      appBar: AppBar(
        primary: true,
        title: Text('Change Address'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(22.0),
        child: Center(
          child: TextField(
            decoration: InputDecoration(
              labelText: 'Address',
            )
          ),
        ),
      )
    ),
  )
);