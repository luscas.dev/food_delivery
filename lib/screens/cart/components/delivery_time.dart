import 'package:flutter/material.dart';
import 'package:flutter_remix/flutter_remix.dart';
import 'package:food_delivery/constants.dart';

Widget deliveryTime(bool show) => AnimatedContainer(
  duration: Duration(milliseconds: 500),
  curve: Curves.easeInQuad,
  transform: Matrix4.translationValues(0, show ? 0 : 300, 0),
  padding: const EdgeInsets.only(top: 34.0, bottom: 10.0, left: 22.0, right: 22.0),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text('Delivery time', style: TextStyle(
        fontSize: 15.0,
        fontWeight: FontWeight.w600,
        color: ThemeColor.grey,
      )),
  
      Row(
        children: [
          Text('Today 11:00 AM', style: TextStyle(
            fontSize: 13.0,
            fontWeight: FontWeight.w400,
            color: ThemeColor.greyVariant,
          )),
          Icon(FlutterRemix.arrow_right_s_line, size: 18.0, color: ThemeColor.greyVariant),
        ],
      ),
    ],
  ),
);