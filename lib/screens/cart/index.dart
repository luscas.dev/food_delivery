import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_remix/flutter_remix.dart';
import 'package:food_delivery/constants.dart';
import 'package:food_delivery/screens/cart/components/delivery_address.dart';
import 'package:food_delivery/screens/cart/components/delivery_time.dart';
import 'package:food_delivery/screens/cart/components/products.dart';

class CartPage extends StatefulWidget {
  const CartPage({ Key? key }) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  bool isStarted = false;
  bool loading = true;

  @override
  initState() {
    Timer(Duration(seconds: 2), () {
      setState(() => this.loading = false);
    });

    Timer(Duration(milliseconds: 300), () {
      setState(() => this.isStarted = true);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(FlutterRemix.arrow_left_s_line),
          color: ThemeColor.grey,
        ),
        title: Text('Cart', style: TextStyle(
          fontSize: 19,
          fontWeight: FontWeight.w700,
          color: ThemeColor.grey,
        )),
        actions: [
          IconButton(
            onPressed: () => print('Menu'),
            icon: Icon(FlutterRemix.menu_3_line),
            color: ThemeColor.grey,
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            child: Flex(
              direction: Axis.vertical,
              children: <Widget>[
                deliveryTime(isStarted),
                deliveryAddress(isStarted),
                Products(show: isStarted)
              ]
            ),
          ),
          
          AnimatedOpacity(
            opacity: loading ? 1.0 : 0.0,
            duration: Duration(milliseconds: 200),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 1.5,
                sigmaY: 1.5,
              ),
              child: Container(
                color: Colors.white70,
                child: Center(child: CircularProgressIndicator()),
              ),
            ),
          ),
        ],
      ),
    );
  }
}